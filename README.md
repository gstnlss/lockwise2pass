# lockwise2pass
Simple perl script to migrate from Firefox Lockwise to [pass](https://www.passwordstore.org/).

## TODO
 - Cleanup the output and give better feedback on what the script is doing.

## Usage
Export your logins from the Lockwise UI, you'll get a csv file. Then just call the `lockwise2pass` script like this:
```sh
$ lockwise2pass <path-to-your-logins.csv>
```
Once the script is done you should have all your passwords stored in your local pass store. The script generates one file for each website you have a password in, in the case of multiple logins for the same website the script generates a folder and places the different passwords inside as a number (`multiple-pass-website.com/1`, `multiple-pass-website.com/2`, etc).

